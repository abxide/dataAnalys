# KisanHub code test

Interview code test for KisanHub

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites


```
Python 3

```

### Installing

```
pip install -r requirements.txt
```

End with an example of getting some data out of the system or using it for a little demo

## Usage

```
python main.py
```

## Running tests
Mostly check if data is correctly downloaded and that the output csv 'weather.csv' is a correct csv file.

```
python test.py
```

## Authors

* **Claudio Procentese (procentese.claudio@gmail.com)** 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details



