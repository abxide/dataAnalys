import matplotlib.pyplot as plt
from sklearn import linear_model

from kisanhub.consts import MONTHS


def uk_data_analysis(frames):
    fig, axs = plt.subplots(ncols=5, figsize=(14, 6))
    for index, frame in enumerate(frames):
        frame_data = frame['data']
        ts = frame_data[MONTHS].mean()
        months = frame_data.columns.values.tolist()
        del months[0]
        upper_band = frame_data[MONTHS].std() * 2
        lower_band = frame_data[MONTHS].std() * -2
        description = frame['description']
        ts.plot(x=MONTHS, kind='bar', title=description, ax=axs[index])
        # errors.describe()
        # upper.plot(ax=axs[index])
        # lower.plot(ax=axs[index])
    plt.show(block=True)


def linear_regression(uk_mean_dataset, england_mean_dataset):
    import statsmodels.api as sm  # import statsmodels

    uk_mean_dataset = uk_mean_dataset.rename(columns={'ANN': 'UK_MEAN'})
    X = uk_mean_dataset[['UK_MEAN']]
    england_mean_dataset = england_mean_dataset.rename(columns={'ANN': 'ENGLAND_MEAN'})
    y = england_mean_dataset[['ENGLAND_MEAN']]
    model = linear_model.LinearRegression()

    X = sm.add_constant(X)
    model = sm.OLS(y, X, missing='drop').fit()
    predictions = model.predict(X)

    model_summary = model.summary()
    print(model_summary)
    fig = plt.figure(figsize=(15, 8))
    fig = sm.graphics.plot_regress_exog(model, "UK_MEAN", fig=fig)
    plt.show(block=True)
