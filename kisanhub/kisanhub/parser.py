import os

from kisanhub.consts import DATA_DIR, OUTPUT_CSV

CSV_FILENAME = DATA_DIR + OUTPUT_CSV


def output_csv(countries):
    print('Writing csv file {}'.format(OUTPUT_CSV))

    os.makedirs(os.path.dirname(CSV_FILENAME), exist_ok=True)
    with open(CSV_FILENAME, 'a') as f:
        # reset file
        f.seek(0)
        f.truncate()
        for key in countries:
            for datadict in countries[key]:
                dataframe = datadict['origin_dataset']
                vertical_format = dataframe.iloc[:, 1:17].stack()
                vertical_format.to_csv(f, encoding='utf-8', index=False)
