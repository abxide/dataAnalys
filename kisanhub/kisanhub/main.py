from kisanhub.consts import DATA_INDEX, COUNTRIES
from kisanhub.data_analysis import linear_regression, uk_data_analysis
from kisanhub.download_data import download_file
from kisanhub.parser import output_csv


def read_data():
    countries = dict()
    for country in COUNTRIES:
        frames = []
        for data in DATA_INDEX:
            frame = download_file(data, country)
            frames.append(frame)
        countries[country] = frames
    return countries


def start_data_analysis(countries):
    uk_data_analysis(countries['UK'])
    uk_mean_data = countries['UK'][2]['data']
    england_mean_data = countries['England'][2]['data']
    linear_regression(uk_mean_data, england_mean_data)


if __name__ == "__main__":
    print("Starting data analysis")
    countries_data = read_data()
    output_csv(countries_data)
    start_data_analysis(countries_data)
