import os

import pandas as pd
import requests

from kisanhub.consts import DATA_DIR, DATA_INDEX, DESCRIPTION_INDEX, ROOT_URL, DEBUG


def parse_dataset(filename, data, country):
    original_dataset = pd.read_csv(filename, skiprows=7, header=0, delim_whitespace=True, na_values=["N/A"])
    original_dataset.fillna('N/A')
    original_dataset.replace({'---': 'N/A'}, inplace=True)
    original_dataset.replace({'': 'N/A'}, inplace=True)
    original_frame = original_dataset.copy()

    new = pd.DataFrame()
    description = DESCRIPTION_INDEX[DATA_INDEX.index(data)]
    new['Year_desc'] = original_dataset.Year.apply(lambda x: country + ',' + description + ',' + str(x))
    dataset_columns = list(original_dataset.columns)
    for column in dataset_columns:
        original_dataset[column] = new['Year_desc'] + ',' + column + ',' + original_dataset[column].astype(str)

    return {'data': original_frame, 'origin_dataset': original_dataset, 'description': description}


def download_file(data, country):
    file_url = ROOT_URL.format(data, country)
    local_filename = country + '.txt'
    print(file_url)
    filename = DATA_DIR + data + '/' + local_filename
    if not DEBUG:
        r = requests.get(file_url, stream=True)
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, 'wb') as f:
            f.write(r.content)

    return parse_dataset(filename, data, country)
