import csv
import unittest

import os

from kisanhub.consts import COUNTRIES, DATA_INDEX, ROOT_URL, DATA_DIR
from kisanhub.parser import CSV_FILENAME


def fun(x):
    return x + 1

class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(fun(3), 4)

    def test_download_data(self):
        for country in COUNTRIES:
            for data in DATA_INDEX:
                local_filename = country + '.txt'
                filename = DATA_DIR + data + '/' + local_filename
                filename = filename.replace('../', '')
                assert os.path.exists(filename) == 1

    def test_validcsv(self):
        filename = CSV_FILENAME.replace('../', '')
        assert os.path.exists(filename) == 1
        with open(filename, newline='') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            self.assertTrue(str(type(csv_reader)), "_csv.reader")
