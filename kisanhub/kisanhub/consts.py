ROOT_URL = 'http://www.metoffice.gov.uk/pub/data/weather/uk/climate/datasets/{}/date/{}.txt'

MONTHS = [
    'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
    'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']

DATA_INDEX = ['Tmax', 'Tmin', 'Tmean', 'Sunshine', 'Rainfall']
DESCRIPTION_INDEX = ['Max temp', 'Min temp', 'Mean temp', 'Sunshine', 'Rainfall']
COUNTRIES = ['UK', 'England', 'Wales', 'Scotland']

DATA_DIR = '../data/'
OUTPUT_CSV = 'weather.csv'
HEADER_CSV = 'region_code,weather_param,year, key, value'

DEBUG=False